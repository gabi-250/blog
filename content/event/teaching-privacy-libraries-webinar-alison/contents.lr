title: Teaching Privacy in Libraries: Webinar with Alison
---
author: steph
---
start_date: 2018-03-29
---
body:

At a time when society is facing a new set of challenges around privacy, surveillance, censorship and free speech, library workers, as stewards of information and providers of internet access, are in a prime position to educate patrons about their digital rights.

Join presenter Alison Macrina, director of the Library Freedom Project, as she discusses practical strategies that can bring privacy back to our library communities at a time when these rights are most at risk. She will demonstrate tools and best practices that can be taught in any library environment, in one-on-one patron interactions or computer classes.

At the end of this one-hour webinar, participants will:

* Learn about standard privacy best practices including: passwords and password managers, endpoint security and software updates
* Become familiar with several of the most trusted privacy technologies including the Tor browser and HTTPS
* Learn how to incorporate privacy literacy into existing computer classes, or teach a standalone privacy class as well as how to stay up to date on privacy news and information

This webinar will be of interest to: All library staff, but particularly those who offer programs around technology.

