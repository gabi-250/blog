title: New Release: Tor Browser 11.0.5 (Android)
---
pub_date: 2022-02-07
---
author: sysrqb
---
categories:

applications
releases
---
summary: Tor Browser 11.0.5 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.0.5 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.0.5/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2021-48/) to Firefox.

Tor Browser 11.0.5 updates Firefox to 94.1.1 and includes bugfixes and
stability improvements.

We use the opportunity as well to update various other components of Tor
Browser: Tor to 0.4.6.9, OpenSSL to 1.1.1m, and NoScript to 11.2.16. We
switch to the latest Go version (1.16.12), too, for building our Go-related
projects.

The full changelog since [Tor Browser 10.5.10](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master) is:

- Android
  * Update Fenix to 94.1.1
  * Update Tor to 0.4.6.9
  * Update NoScript to 11.2.16
  * Update OpenSSL to 1.1.1m
  * [Bug tor-android-service#40006](https://gitlab.torproject.org/tpo/applications/tor-android-service/-/issues/40006): Add new default obfs4 bridge "deusexmachina"
  - [Bug fenix#40198](https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40198): Spoof English toggle now overlaps with locale list
  - [Bug tor-browser-build#40393](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40393): Point to a forked version of pion/dtls with fingerprinting fix
  - [Bug tor-browser-build#40394](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40394): Bump version of Snowflake to 221f1c41
  - [Bug tor-browser-build#40398](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40398): Jetify tor-android-service packages
  - [Bug tor-browser#40682](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40682): Disable network.proxy.allow_bypass
  - [Bug tor-browser#40736](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40736): Disable third-party cookies in Private Browsing Mode
- Build System
  - Android
    - [Bug tor-browser-build#40403](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40403): Update Go to 1.16.12
    - [Bug tor-browser-build#40366](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40366): Use bullseye to build https-everywhere
    - [Bug tor-browser-build#40368](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40368): Use system's python3 for android builds
    - [Bug tor-browser-build#40373](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40373): Update components for mozilla93
    - [Bug tor-browser-build#40379](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40379): Update components for mozilla94
    - [Bug tor-browser-build#40395](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40395): Update node to 12.22.1
