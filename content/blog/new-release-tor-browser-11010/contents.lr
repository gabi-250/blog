title: New Release: Tor Browser 11.0.10 (Windows, macOS, Linux)
---
pub_date: 2022-04-07
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.0.10 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.0.10 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.0.10/).

This version includes important security updates to Firefox:
- https://www.mozilla.org/en-US/security/advisories/mfsa2022-14/

Tor Browser 11.0.10 updates Firefox on Windows, macOS, and Linux to 91.8.0esr.

We use the opportunity as well to update various other components of Tor Browser:
- NoScript 11.4.3

The full changelog since [Tor Browser 11.0.9](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.0) is:

- Windows + OS X + Linux
  - Update Firefox to 91.8.0esr
  - Update NoScript to 11.4.3
  - [Bug tor-browser#14939](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/14939): Support ipv6 address in Tor Circuit Display
  - [Bug tor-browser-build#40469](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40469): Update zlib to 1.2.12 (CVE-2018-25032)
  - [Bug tor-browser#40718](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40718): Application Menu items should be sentence case
  - [Bug tor-browser#40725](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40725): about:torconnect missing identity block content on TB11
  - [Bug tor-browser#40776](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40776): Capitalize the "T" in "Tor" in application menu
  - [Bug tor-browser#40862](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40862): Backport 1760674
