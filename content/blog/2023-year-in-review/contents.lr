title: 2023: Year in Review
---
author: pavel
---
pub_date: 2023-12-29
---
categories:
fundraising
community
network
applications
circumvention
human rights
advocacy

---
summary: 
As we bid farewell to 2023, we reflect on the hard work of the Tor Project's teams and their many noteworthy achievements to improve Tor and its experience for millions of users all around the world. Thank you to our community of users, volunteers, relay operators, partners, and donors for making these projects possible through their generous contributions.

---
body:

As we bid farewell to an eventful year, it's time to reflect on the hard work of the Tor Project's teams and their many noteworthy achievements to improve Tor and its experience for millions of users all around the world–whether it is by making the Tor network more performant and secure, rewriting Tor's code base to make it easier to maintain and embed in new and third party applications, or making Tor technology more widely available through localization, outreach, advocacy, and training. A lot of these projects would not have been possible were it not for the generous contributions of our donors, supporters and volunteers.

The Tor Project's ability to push out many new releases this year is the direct result of countless hours spent by volunteers from all over the world testing new tools and providing us with invaluable feedback that has helped us improve and deploy new features. We could not have done this work without your unwavering support. Thank you to our community of users, volunteers, relay operators, partners, and donors for making 2023 a year of many achievements.
 
![Mullvad Browser key visual](Mullvad-Browser-keyvisual-001.png)

# The launch of Mullvad Browser - Tor Browser without Tor
We've teamed up with Mullvad VPN to [launch Mullvad Browser](https://blog.torproject.org/releasing-mullvad-browser/), a privacy-focused web browser built on the principles of the Tor Browser. It offers similar privacy features to Tor Browser, such as anti-fingerprinting and anti-tracking protections, but works independently of the Tor network. Tor Browser will continue to exist as a powerful option for anonymity online, while Mullvad Browser provides an alternative privacy browser.

At the Tor Project, we offer an example of how technology can be made differently. Mullvad Browser is a great example that it is possible to build technology used by millions of people with privacy at the heart.

This partnership has allowed us to make necessary improvements to Tor Browser benefiting both browsers--and the larger privacy tech ecosystem. The Tor Project teams can more freely develop experimental features, catering to the Mullvad Browser alpha channel users without compromising the stringent anonymity standards of Tor Browser.

# Tor Browser
Some of those beneficial knock-on effects have already come to life in the new features of this year's Tor Browser releases 12.5 and 13. The applications teams were able to [address numerous legacy issues](https://blog.torproject.org/new-release-tor-browser-125/) and conduct an accessibility review of Tor Browser's custom components. Particularly notable are the accessibility improvements that were gained as a result of the [transition to Firefox ESR 115 alongside visual changes to the user interface](https://blog.torproject.org/new-release-tor-browser-130/) that promise to improve performance significantly for people who use screen readers and other assistive technology.

The 2023 releases aimed to create a mature user experience, pushing Tor Browser even more towards the mainstream in terms of quality and UI, featuring updated circuit display, new icons for .onion sites, improvements to the connection experience, refreshed application icons, homepage features, and bigger new windows. The latest release also marks the [removal of legacy code associated with the Torbutton](https://blog.torproject.org/torbutton-retired/)--a step towards future-proofing Tor's codebase and providing better integration and a more seamless transition to the new Arti implementation.

# Arti
Speaking of the Arti project, our multi-year project to rewrite Tor in Rust, 2023 saw the completion of anti-censorship support offering fully functional bridges and pluggable transports. The team also performed extensive work on onion services, paving the way for usable client support and imminent server-side readiness, and collaborated with the applications team to explore various API designs, setting up Arti as the foundation for various programs and services beyond just the Tor Browser. Aside from streamlining maintainability and scalability, Arti also addresses security and performance concerns alongside the many efforts of the network team. 

![Tor network key visual](tor-network.png)

# Network team

In 2023, the network team deployed critical new features to improve the Tor network's defense mechanisms, speed, and performance. Congestion Control and Conflux, two designs aimed at optimizing the flow of traffic by selecting less congested relays and creating multiple paths through the network, are already showing promising results, including a 2X multiplier for download speeds as well as a more stable user experience through longer lived connections.

The team also put a lot of work into [mitigating attacks on the Tor network](https://blog.torproject.org/tor-network-ddos-attack/) and enhancing its defenses for onion services, culminating in the introduction of a [proof-of-work (PoW) defense](https://blog.torproject.org/introducing-proof-of-work-defense-for-onion-services/) in the release of Tor 0.4.8. This dynamic PoW mechanism prioritizes traffic from real users, deterring attackers and making large-scale DoS attacks impractical for adversaries.

These robust solutions that dynamically adjust to traffic demands, are the direct result of user feedback, sponsored projects and volunteer contributions. We want to acknowledge the Relay Operator Community's swift deployment by upgrading to 0.4.8 during feature releases, proving once again that the Tor network's strength lies in its collaborative spirit to meet the growing demands of our millions of users.

![Collage of photos featuring Tor team members presenting at Tor training sessions](community-outreach.png)

# Community work & outreach
But building a trusted network is not just a matter of iterating and improving on technological solutions but also maintaining this collaborative spirit within our community of volunteers, relay operators, allies, donors, and sponsors. Throughout the year, the Community Team has executed comprehensive programs emphasizing community building, education, and proactive initiatives to fortify the [Tor Project's global impact](https://blog.torproject.org/furthering-our-mission-in-the-global-south/).

Regular check-ins with Tor's relay operator community, whether at our monthly meetups, one-on-one meetings at events, dedicated chat channels, the [Tor Forum](https://blog.torproject.org/tor-project-forum-migration/) or active mailing lists, aim at [introducing ideas, promoting initiatives, and establishing consensus.](http://community.torproject.org/policies) This unity is important to [uphold the strength of our distributed network](https://blog.torproject.org/tor-network-community-health-update/) and defend against malicious actors.

As part of our [outreach into communities](https://blog.torproject.org/empowering-human-rights-defenders/), the Tor Project has conducted digital security and Tor trainings with journalists, activists, human rights defenders, and lawyers globally. At numerous online and offline sessions, we have engaged participants from Global majority countries and organized regional meetups in Eastern Africa and Latin America to share updates and address specific challenges faced by our partners, and use these insights to introduce new features.

A standout event of the year was the Tor Training Academy, bringing together over 20 digital security practitioners in a two-day, in-person boot camp. The academy aimed to train practitioners to teach Tor, covering the nuances of Tor's network and tool ecosystem. Guest speakers from the Open Observatory of Network Interference addressed network tampering measurement, while Localization Lab presented their efforts to make Tor tools more widely available by localizing them into more languages. To that end, we've also created [a localized video series](https://blog.torproject.org/tor-tutorials/) in Swahili, Chinese and Arabic that features simple and easy-to-understand educational content on how to use Tor browser, Bridges and OnionShare.

Yet, many more users are still barred from accessing the privacy and anonymity benefits the Tor network offers due to deliberate attempts from bad actors to block access to it.

# Anti-censorship work
Fortunately, the Tor Project's anti-censorship team has learned a lot from responding to large-scale censorship events in previous years, testing and identifying weaknesses in our existing censorship-resistant infrastructure. This year, we directed a lot of our time and energy to improve some of those systems, expanding and improving the tools in our toolkit so that we can better respond to censorship events as they occur. 

We focused on developing and testing new tools, WebTunnel and Conjure. WebTunnel is a pluggable transport that makes traffic appear as a regular secure connection to a website making it difficult for censors to determine whether the website is also a WebTunnel bridge. Conjure takes advantage of the large number of unused IPv6 addresses in a friendly ISP providers address space. Since this space is very large and the connection address can keep changing, it is difficult for a censor to block all potential "phantom" proxies without incurring significant collateral damage against real websites hosted by the deploying ISPs.

A major challenge that our team faced in 2023 was assisting with the censorship situation in Turkmenistan. This really pushed the limits of Tor's existing anti-censorship tools. Many of our strategies were not as effective as in other regions since the Turkmen censors did not shy away from blocking entire IP blocks. While we were able to support some users with meek and obfs4 bridges, some of the providers that Tor relies on for domain fronting will be discontinuing their services in 2024. 

This illustrates why we need to continuously invest in building out our anti-censorship response and developing new tools to stay ahead of adversaries in the highly dynamic and ever-changing censorship landscape. The more tools we have at our disposal, the better we will be able to target our response, keeping censors at bay and [enabling millions of users to access the free and open internet](https://blog.torproject.org/support-tor-project-share-your-story/).

___

Our goal is to ensure that Tor works for everyone. Amid geopolitical conflicts and climate disasters that put millions of people at risk, the internet has become crucial for us to communicate, to learn about what is happening around the world, to organize, to defend human rights, and to build solidarity. That is why contributions like yours are important to keep Tor strong.

[![Donate Button](yec-button.png "class=align-center")](https://torproject.org/donate/donate-bp6-2023)

Get engaged, run more bridges, proxies and relays if you can to continue our fight against censorship and for free and open access to the unrestricted internet.