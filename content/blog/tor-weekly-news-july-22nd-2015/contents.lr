title: Tor Weekly News — July 22nd, 2015
---
pub_date: 2015-07-22
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twenty-ninth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>More TSoP status reports</h1>

<p>The students in this year’s Tor Summer of Privacy continued work on their respective projects, as their status reports show. </p>

<p>Jesse Victors made <a href="https://lists.torproject.org/pipermail/tor-dev/2015-July/009103.html" rel="nofollow">significant progress</a> on his DNS-like Onion Naming System (OnioNS) project at the recent onion service development meeting in Washington, DC. Many bugs were fixed and the software is now in a demonstration-ready state. The issue of implementing a global source of randomness, which is important for the <a href="https://gitweb.torproject.org/torspec.git/tree/proposals/224-rend-spec-ng.txt" rel="nofollow">next generation of onion services</a> as well as for OnionNS, was also worked on. “The server-to-server communication needs a few bug fixes, but most of that code is in place. As soon as that is complete, I should be about ready for a beta test.”</p>

<p>Israel Leiva sent the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000878.html" rel="nofollow">first report</a> on his GetTor enhancement project. GetTor now distributes links to copies of Tor Browser hosted on Github as well as Dropbox, and the text of the autoresponder was expanded with more information. Upcoming additions include a Google Drive script, distribution of mirror links, and the promotion of Github to the default download source.</p>

<p>Cristobal Leiva also sent his <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000879.html" rel="nofollow">first report</a>, for the relay web status dashboard project. A prototype UI has been <a href="https://github.com/leivaburto/rwsd/issues/8" rel="nofollow">created</a> and development milestones have been prioritized: “Over the next two weeks I’ll be coding the graph and log components”.</p>

<p>Finally, Donncha O’Cearbhaill’s OnionBalance load-balancing system <a href="https://lists.torproject.org/pipermail/tor-reports/2015-July/000880.html" rel="nofollow"></a> has been enhanced with unit tests, and has received some real-world testing courtesy of s7r <a href="https://lists.torproject.org/pipermail/tor-talk/2015-July/038373.html" rel="nofollow"></a>.</p>

<p>Exciting progress all round!</p>

<h1>Miscellaneous news</h1>

<p>The Guardian Project announced new releases of Chatsecure and Orbot. <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2015-July/004452.html" rel="nofollow">Chatsecure v14.2.0</a> is “all about squashing bugs, reducing memory and improving network stability”, while <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2015-July/004453.html" rel="nofollow">Orbot 15.0.3-RC-3</a> features “overall improvements to system and server stability, improvements to Apps VPN mode support…improved launch and hidden service API for third-party app interaction”, and updates to Tor and OpenSSL.</p>

<p>Anthony G. Basile <a href="https://lists.torproject.org/pipermail/tor-talk/2015-July/038493.html" rel="nofollow">announced</a> a new release of tor-ramdisk, the micro Linux distribution whose only purpose is to host a Tor server in an environment that maximizes security and privacy. Version 20150714 includes updates to the distribution’s core software.</p>

<p>Nick Mathewson published <a href="https://lists.torproject.org/pipermail/tor-dev/2015-July/009088.html" rel="nofollow">proposal 248</a> , which offers a migration path for “finally removing our old Ed25519 keys”.</p>

<p>Following the recent outage of <a href="https://lists.torproject.org/pipermail/tor-talk/2015-July/038487.html" rel="nofollow">meek-azure</a>, David Fifield published a <a href="https://lists.torproject.org/pipermail/tor-talk/2015-July/038496.html" rel="nofollow">workaround</a> for those who want to use this backend.</p>

<p>The Tails team is <a href="https://mailman.boum.org/pipermail/tails-dev/2015-July/009238.html" rel="nofollow">planning</a> a sprint in November, possibly face-to-face, focusing on porting Tails to the current stable release of Debian, Jessie. “If you want to join the fun, let me know. If you’re interested in having a face-to-face sprint to work on this in November, let me know. If these dates don’t work for you, let me know”, wrote intrigeri.</p>

<p>The Intercept’s Micah Lee published a detailed <a href="https://firstlook.org/theintercept/2015/07/14/communicating-secret-watched/" rel="nofollow">beginner’s guide to secure online chat</a>, including how to configure your chat client to use Tor.</p>

<p>For those who wish the modern world looked more like “Johnny Mnemonic” than “CITIZENFOUR”: you can hear Keanu Reeves narrating an <a href="https://vimeo.com/124777509" rel="nofollow">unexpectedly soothing animation</a> about the “Deep Web” and Tor onion services as part of Alex Winter’s upcoming <a href="http://www.deepwebthemovie.com/" rel="nofollow">documentary film on the subject</a>.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

