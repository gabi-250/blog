title: Tails 1.4 is out
---
pub_date: 2015-05-12
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 1.4, is out.</p>

<p>This release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_1.3.2/" rel="nofollow">numerous security issues</a> and all users must <a href="https://tails.boum.org/doc/first_steps/upgrade/" rel="nofollow">upgrade</a> as soon as possible.</p>

<h2>New features</h2>

<ul>
<li><em>Tor Browser</em> 4.5 now has a <strong><a href="https://tails.boum.org/doc/anonymous_internet/Tor_Browser/#security_slider" rel="nofollow">security slider</a></strong> that you can use to disable browser features, such as JavaScript, as a trade-off between security and usability. The security slider is set to <em>low</em> by default to provide the same level of security as previous versions and the most usable experience.
<p>We disabled in Tails the new circuit view of <em>Tor Browser</em> 4.5 for security reasons. You can still use the network map of <em>Vidalia</em> to inspect your circuits.</p></li>
<li><em>Tails OpenPGP Applet</em> now has a <strong>shortcut to the <em>gedit</em> text editor</strong>, thanks to Ivan Bliminse.</li>
<li><strong><em><a href="https://tails.boum.org/doc/advanced_topics/paperkey/" rel="nofollow">Paperkey</a></em></strong> lets you print a backup of your OpenPGP secret keys on paper.</li>
</ul>

<h2>Upgrades and changes</h2>

<ul>
<li><em>Tor Browser</em> 4.5 protects better against <strong>third-party tracking</strong>. Often when visiting a website, many connections are created to transfer both the content of the main website (its page, images, and so on) and third-party content from other websites (advertisements, <em>Like</em> buttons, and so on). In <em>Tor Browser</em> 4.5, all such content, from the main website as well as the third-party websites, goes through the same Tor circuits. And these circuits are not reused when visiting a different website. This prevents third-party websites from correlating your visits to different websites.</li>
<li><em>Tor Browser</em> 4.5 now keeps using the <strong>same Tor circuit</strong> while you are visiting a website. This prevents the website from suddenly changing language, behavior, or logging you out.</li>
<li><strong><em><a href="https://search.disconnect.me/" rel="nofollow">Disconnect</a></em></strong> is the new <strong>default search engine</strong>. <em>Disconnect</em> provides Google search results to Tor users without captchas or bans.</li>
<li>Better support for <strong>Vietnamese</strong> in <em>LibreOffice</em> through the installation of <span class="geshifilter"><code class="php geshifilter-php">fonts<span style="color: #339933;">-</span>linuxlibertine</code></span>.</li>
<li>Disable security warnings when connecting to POP3 and IMAP ports that are mostly used for StartTLS nowadays.</li>
<li>Support for <strong>more printers</strong> through the installation of <span class="geshifilter"><code class="php geshifilter-php">printer<span style="color: #339933;">-</span>driver<span style="color: #339933;">-</span>gutenprint</code></span>.</li>
<li>Upgrade <strong>Tor</strong> to 0.2.6.7.</li>
<li>Upgrade <strong><em>I2P</em></strong> to <a href="https://geti2p.net/en/blog/post/2015/04/12/0.9.19-Release" rel="nofollow">0.9.19</a> that has several fixes and improvements for floodfill performance.</li>
<li>Remove the obsolete <strong>#i2p-help IRC channel</strong> from <em>Pidgin</em>.</li>
<li>Remove the command line email client <strong><span class="geshifilter"><code class="php geshifilter-php">mutt</code></span></strong> and <strong><span class="geshifilter"><code class="php geshifilter-php">msmtp</code></span></strong>.</li>
</ul>

<p>There are numerous other changes that might not be apparent in the daily operation of a typical user. Technical details of all the changes are listed in the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">Changelog</a>.</p>

<h2>Fixed problems</h2>

<ul>
<li>Make the browser theme of the Windows 8 camouflage compatible with the <em>Unsafe Browser</em> and the <em>I2P Browser</em>.</li>
<li>Remove the <strong>Tor Network Settings...</strong> from the <em>Torbutton</em> menu.</li>
<li>Better support for Chromebook C720-2800 through the upgrade of <span class="geshifilter"><code class="php geshifilter-php">syslinux</code></span>.</li>
<li>Fix the localization of <em>Tails Upgrader</em>.</li>
<li>Fix the OpenPGP key servers configured in <em>Seahorse</em>.</li>
<li>Prevent <em>Tor Browser</em> from crashing when <em>Orca</em> is enabled.</li>
</ul>

<h1>Known issues</h1>

<ul>
<li>Claws Mail stores plaintext copies of all emails on the remote IMAP server, including those that are meant to be encrypted. If you send OpenPGP encrypted emails using <em>Claws Mail</em> and IMAP, make sure to apply one of the workarounds documented in our <a href="https://tails.boum.org/security/claws_mail_leaks_plaintext_to_imap/" rel="nofollow">security announcement</a>.</li>
<li>See the current list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">known issues</a>.</li>
</ul>

<h1>Download or upgrade</h1>

<p>Go to the <a href="https://tails.boum.org/download/" rel="nofollow">download</a> page.</p>

<h1>What's coming up?</h1>

<p>The next Tails release is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for June 30.</p>

<p>Have a look to our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Do you want to help? There are many ways <a href="https://tails.boum.org/contribute/" rel="nofollow"><strong>you</strong> can contribute to Tails</a>. If you want to help, come talk to us!</p>

