title: A closer look at online privacy: new Tor tutorials
---
author: pavel
---
pub_date: 2023-09-30
---
categories: 

advocacy
applications
circumvention
community
human rights
localization
usability
---
summary: Think using Tor is difficult? Think again! We've created a short, easy-to-digest video series about Tor Browser, bridges and Onion Share.
---
body:

At Tor, we're always looking for ways to [empower more people to access the unrestricted internet](https://blog.torproject.org/empowering-human-rights-defenders/) and defend against surveillance and censorship with our digital tools. To achieve this goal, we've taken significant steps to ensure that our tools are user-friendly and accessible to a wide audience. That includes localization of our offering into as many languages as possible.

As part of a recently completed project, we've made Tor Browser, Tor circumvention tools, Tor documentation and training materials, and OnionShare available in Arabic, Chinese, and Swahili. Additionally, we've developed short, localized, and easy-to-digest explainer videos that guide users on how to access the [Tor network](https://www.youtube.com/playlist?list=PLwyU2dZ3LJEpKJ6EsZ0g8-HmrPgOZ1qTF), [bypass censorship](https://www.youtube.com/playlist?list=PLwyU2dZ3LJEp_9gfn3EwO8ORkvSn0JYDg), and [share files](https://www.youtube.com/playlist?list=PLwyU2dZ3LJEoTzE6v4SAZNGl2Kv4MuePT) securely and anonymously.

## Think using Tor is difficult? Think again!
Our focus is not solely on developing anti-censorship and anti-surveillance technology, but also on increasing awareness of these tools. The video series was designed to showcase the ease-of-use of our most popular tools and combat misconceptions about their everyday use. They serve as user-friendly guides on how to incorporate these tools into daily life and are crucial building blocks in our coordinated approach to reaching activists, journalists, human rights defenders and civil society groups.

If you've ever been asked [how Tor Browser works](https://youtu.be/qYcErJc9N3o), or wanted to learn about [censorship circumvention tools like Bridges](https://youtu.be/8mdtSgHWhXY) and how to [anonymously share files](https://youtu.be/gCDbHkpkNJ0), our how-to videos can help you get started easily. Please feel free to use these videos in your own outreach and advocacy work, and share them with anyone interested in learning more about safeguarding their online privacy and security with Tor.

We would like to thank the [International Republican Institute (IRI)](https://www.iri.org/) for sponsoring this project and helping us further reduce barriers to the adoption of our technology among vulnerable communities. Localizing our technology and its supporting documentation, including videos like these, are valuable additions to our outreach and training efforts.