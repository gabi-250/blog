title: Tor Weekly News — June 11th, 2015
---
pub_date: 2015-06-11
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twenty-third issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Blocking-resistant communication through domain fronting</h1>

<p>David Fifield, lead developer of the <a href="https://trac.torproject.org/projects/tor/wiki/doc/meek" rel="nofollow">meek</a> pluggable transport, co-authored a <a href="https://lists.torproject.org/pipermail/tor-dev/2015-June/008940.html" rel="nofollow">paper</a> entitled “Blocking-resistant communication through domain fronting”, the technique that meek (along with other software such as Lantern and Psiphon) uses to ensure that its connections to the Tor network can’t be blocked by a censor without incurring significant collateral damage. The paper is based on the experience of operating domain-fronting systems on the real network. Congratulations to the researchers on this milestone in their project!</p>

<p>David also published the monthly <a href="https://lists.torproject.org/pipermail/tor-dev/2015-June/008932.html" rel="nofollow">summary of costs</a> incurred by the infrastructure for meek. Rate-limits have now been imposed on some backends in order to keep the operating costs sustainable.  This sophisticated pluggable transport is a vital tool for thousands of users in areas that censor all other circumvention systems, so if you know of (or are) a friendly funder looking for a worthy project to support, please let the community know!</p>

<h1>The Art of Dissent</h1>

<p>Laura Poitras, Pulitzer Prize-winning director of the Academy Award-winning “Citizenfour” and <a href="http://media.ccc.de/browse/congress/2014/31c3_-_6251_-_en_-_saal_1_-_201412301400_-_state_of_the_onion_-_jacob_-_arma.html" rel="nofollow">public advocate for Tor and Tails</a>, documented a collaboration between the Tor Project’s Jacob Appelbaum and artist Ai Weiwei — both of them “artists, journalists, dissidents, polymaths — and targets” — involving a “zone of hyper-surveillance”, shredded NSA documents, and several cuddly toy pandas.</p>

<p>Laura’s film is available to view on the <a href="http://www.nytimes.com/video/opinion/100000003729935/the-art-of-dissent.html" rel="nofollow">New York Times website</a> as part of its “Op-Docs” series, accompanied by an article in which Laura describes her interest in “the way being watched and recorded affects how we act, and how watching the watchers, or counter-surveillance, can shift power”.</p>

<h1>More monthly status reports for May 2015</h1>

<p>The wave of regular monthly reports from Tor project members for the month of May continued, with reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000847.html" rel="nofollow">Isis Lovecruft</a> (maintaining Tor’s bridge distribution system along with other coding/organizational work), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000848.html" rel="nofollow">Israel Leiva</a> (leading the GetTor project), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000850.html" rel="nofollow">Sukhbir Singh</a> (working on Tor Messenger, TorBirdy, and GetTor), and <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000851.html" rel="nofollow">Arlo Breault</a> (also developing Tor Messenger, and maintaining Tor Check).</p>

<p>The Tails team published its <a href="https://tails.boum.org/news/report_2015_05/" rel="nofollow">monthly report</a>, while George Kadianakis sent out the report for <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000852.html" rel="nofollow">SponsorR</a>, and Arturo Filastò reported on the OONI team’s <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000853.html" rel="nofollow">May progress</a>.</p>

<p>Tor Summer of Privacy Students <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000849.html" rel="nofollow">Donncha O’Cearbhaill</a> and <a href="https://lists.torproject.org/pipermail/tor-dev/2015-June/008935.html" rel="nofollow">Jesse Victors</a> also sent out their first project status reports.</p>

<h1>Miscellaneous news</h1>

<p>Giovanni Pellerano <a href="https://lists.torproject.org/pipermail/tor-talk/2015-June/038108.html" rel="nofollow">announced</a> version 3.1.41 of Tor2web, the tool for non-anonymous connections to onion services from regular browsers. This version of the software allows users to download Tor Browser from any running Tor2web instance, as well as other improvements. See Giovanni’s message for more details.</p>

<p>Donncha O’Cearbhaill published the <a href="https://lists.torproject.org/pipermail/tor-talk/2015-June/038058.html" rel="nofollow">results</a> of his call for input from onion service operators on the challenges of running high-performance sites and services through a Tor process.</p>

<p>teor published a <a href="https://lists.torproject.org/pipermail/tor-dev/2015-June/008924.html" rel="nofollow">guide</a> to obtaining a copy of OpenSSL on Mac OS X that is recent enough to build a working copy of Tor.</p>

<p>Thanks to Justaguy for running another <a href="https://lists.torproject.org/pipermail/tor-mirrors/2015-June/000903.html" rel="nofollow">mirror</a> of the Tor Project website and software archive!</p>

<p>This issue of Tor Weekly News has been assembled by Harmony and the Tails developers.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

