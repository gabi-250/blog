title: Tor is recruiting an Executive Administrator (a talented, organized office person)
---
pub_date: 2015-05-27
---
author: ailanthus
---
tags:

jobs
Boston
---
categories: jobs
---
_html_body:

<p>The Tor Project is evaluating our needs and resources as we transition to a new era and begin our search for a new Executive Director. We have decided that we need better systems for sharing information and coordinating the incredibly diverse work of the Tor community.</p>

<p>We are recruiting an executive administrator to be at the hub of our leadership team. We seek an intuitive problem solver who can impose just the right amount of order on the ways that internal Tor communicates, makes decisions, and generally takes care of business. Our geographically and functionally diverse team has many stakeholders. A person who can step in to build processes, make connections, and provide support from all angles will be essential. The successful applicant will have the ability to translate objectives from diverse teams to a central framework that promotes better communication and support for all.</p>

<p>We hope and expect that applicants from many kinds of backgrounds will apply. We are not likely to find one person who meets all of our criteria for this role, but we recognize that particular strengths will make up for lack of experience in other areas. For example, a strong background in project management within the free and open source software movement might overcome lack of experience in formal office administration. We invite each candidate to make the case for how his or her skill set, knowledge, and attitude would assist in fulfilling the overall objectives of the role.</p>

<p>Please review the posting here: <a href="https://www.torproject.org/about/jobs-execadmin.html.en" rel="nofollow">https://www.torproject.org/about/jobs-execadmin.html.en</a>  and consider sharing it within your networks. This is a phenomenal opportunity for a motivated, organized professional to make an immediate impact working at the forefront of anonymous and secure communications!</p>

---
_comments:

<a id="comment-94127"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94127" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 27, 2015</p>
    </div>
    <a href="#comment-94127">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94127" class="permalink" rel="bookmark">Where may I find more</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Where may I find more information about this job?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94128"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94128" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 27, 2015</p>
    </div>
    <a href="#comment-94128">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94128" class="permalink" rel="bookmark">Remote?  Local?  Salary</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Remote?  Local?  Salary range?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94130"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94130" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  ailanthus
  </article>
    <div class="comment-header">
      <p class="comment__submitted">ailanthus said:</p>
      <p class="date-time">May 27, 2015</p>
    </div>
    <a href="#comment-94130">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94130" class="permalink" rel="bookmark">Here is the job</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Here is the job announcement:<br />
<a href="https://www.torproject.org/about/jobs-execadmin.html.en" rel="nofollow">https://www.torproject.org/about/jobs-execadmin.html.en</a><br />
--The job is based in our Cambridge, MA office.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94148"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94148" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 28, 2015</p>
    </div>
    <a href="#comment-94148">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94148" class="permalink" rel="bookmark">Aren&#039;t talented and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Aren't talented and organized usually antagonists? I would say organized is the right characteristic for the role while talented is probably going to complicate matters.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-94459"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94459" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 06, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-94148" class="permalink" rel="bookmark">Aren&#039;t talented and</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-94459">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94459" class="permalink" rel="bookmark">Talented at organization. K-D</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Talented at organization. K-D</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-94152"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94152" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 28, 2015</p>
    </div>
    <a href="#comment-94152">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94152" class="permalink" rel="bookmark">What about people from</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What about people from abroad who are NOT US citizens? Is that a problem?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94156"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94156" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 28, 2015</p>
    </div>
    <a href="#comment-94156">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94156" class="permalink" rel="bookmark">is this job available only</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>is this job available only for north american candidates?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94159"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94159" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 28, 2015</p>
    </div>
    <a href="#comment-94159">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94159" class="permalink" rel="bookmark">Good luck getting a work visa</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good luck getting a work visa</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94194"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94194" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 30, 2015</p>
    </div>
    <a href="#comment-94194">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94194" class="permalink" rel="bookmark">Do you hire persons who are</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do you hire persons who are not US citizens for jobs based in Cambridge?<br />
What do you think of people who never lived in the US and would move to the US for the job?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94203"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94203" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 30, 2015</p>
    </div>
    <a href="#comment-94203">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94203" class="permalink" rel="bookmark">Hi,
Any plans for core C++</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,</p>
<p>Any plans for core C++ engineering jobs?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94221"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94221" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 30, 2015</p>
    </div>
    <a href="#comment-94221">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94221" class="permalink" rel="bookmark">Does past Computer Science</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does past Computer Science experience play a big role?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94295"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94295" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 02, 2015</p>
    </div>
    <a href="#comment-94295">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94295" class="permalink" rel="bookmark">Do you have a secure and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do you have a secure and encrypted way to apply?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94363"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94363" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 04, 2015</p>
    </div>
    <a href="#comment-94363">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94363" class="permalink" rel="bookmark">Tor Project = NSA / ZOG :(</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor Project = NSA / ZOG :(</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-94419"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-94419" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 06, 2015</p>
    </div>
    <a href="#comment-94419">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-94419" class="permalink" rel="bookmark">Sustain the remarkable work</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sustain the remarkable work !! Lovin' it!</p>
</div>
  </div>
</article>
<!-- Comment END -->
