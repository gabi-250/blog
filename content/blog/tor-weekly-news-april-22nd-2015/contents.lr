title: Tor Weekly News — April 22nd, 2015
---
pub_date: 2015-04-22
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the sixteenth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>A new era at the Tor Project</h1>

<p>Andrew Lewman, who has overseen the Tor Project’s business operations since 2009 as Executive Director, will shortly be <a href="https://blog.torproject.org/blog/new-era-tor-project" rel="nofollow">moving on</a> to a new position at the head of an internet services company. During Andrew’s time as Executive Director, Tor has developed into a leading privacy tool used every day by millions of users around the world, and the Tor community has grown to include more engineers, researchers, activists, and volunteers than ever before. Thanks to Andrew for everything he has done for the Tor community, and best of luck for the future!</p>

<h1>Tor Summer of Privacy — chosen projects announced!</h1>

<p>The application period for the first-ever <a href="https://trac.torproject.org/projects/tor/wiki/org/TorSoP" rel="nofollow">Tor Summer of Privacy</a> — the funded development season inspired by Google Summer of Code — closed last week, with twenty-two proposals submitted before the deadline. All the project ideas were of a very high standard, but sadly funding and mentoring constraints meant that only four could be chosen, and Damian Johnson took to the <a href="https://blog.torproject.org/blog/tor-summer-privacy-projects" rel="nofollow">Tor blog</a> to share the list of the winners.</p>

<p>Donncha O’Cearbhaill will be implementing a <a href="https://gist.github.com/DonnchaC/03ad5cd0b8ead0ae9e30" rel="nofollow">system</a> to increase the availability of large onion services by balancing requests across several back-end servers, each running its own Tor instance; Jesse Victors will be working on a project entitled “The Onion Name System: Tor-Powered Distributed DNS for Tor Hidden Services”, based on his <a href="https://github.com/Jesse-V/Thesis" rel="nofollow">thesis</a>; former GSoC student and GetTor project leader Israel Leiva will be returning to carry out <a href="https://people.torproject.org/~ilv/sop_proposal_2015.html" rel="nofollow">further work</a> on the alternative distribution system for Tor software; and Israel’s twin brother Cristóbal will be developing a web-based <a href="https://leivaburto.github.io/sop-proposal/" rel="nofollow">status dashboard for Tor relays</a>.</p>

<p>These are all exciting and important projects, and as Damian wrote, “we’re thrilled to have them with us”. Even if you submitted a proposal that wasn’t chosen, please don’t be discouraged: a number of the projects rejected for lack of resources are still high on Tor developers’ wishlists, so if you can find any way to contribute in the future, feel free to seek advice and assistance from the community.</p>

<p>Coding for Tor Summer of Privacy officially starts on May 25th.  In the meantime, congratulations to the four students!</p>

<h1>Miscellaneous news</h1>

<p>Yawning Angel <a href="https://lists.torproject.org/pipermail/tor-dev/2015-April/008665.html" rel="nofollow">announced</a> obfs4proxy-0.0.5. This release is better able to detect whether the parent Tor process has crashed, and also brings IPv6 support to clients. All users are recommended to upgrade: please see Yawning’s announcement for further details.</p>

<p>Anthony G. Basile <a href="https://lists.torproject.org/pipermail/tor-talk/2015-April/037497.html" rel="nofollow">announced</a> version 20150411 of Tor-ramdisk, the micro Linux distribution whose only purpose is to host a Tor server in an environment that maximizes security and privacy. This release includes updated versions of Tor and the Linux kernel.</p>

<p>David Fifield <a href="https://lists.torproject.org/pipermail/tor-dev/2015-April/008705.html" rel="nofollow">wondered</a> about the cause of a sudden large increase in the number of Tor clients using the meek pluggable transport that occurred around April 15th.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

