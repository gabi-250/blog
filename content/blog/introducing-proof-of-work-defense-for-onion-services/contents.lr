title: Introducing Proof-of-Work Defense for Onion Services
---
author: pavel
---
pub_date: 2023-08-23
---
categories: 

network
onion services
releases
usability
---
summary: Today, we are officially introducing a proof-of-work (PoW) defense for onion services designed to prioritize verified network traffic as a deterrent against denial of service (DoS) attacks with the release of Tor 0.4.8.
---
body:

Today, we are officially introducing a proof-of-work (PoW) defense for onion services designed to prioritize verified network traffic as a deterrent against denial of service (DoS) attacks with the release of Tor 0.4.8.

Tor's PoW defense is a dynamic and reactive mechanism, remaining dormant under normal use conditions to ensure a seamless user experience, but when an onion service is under stress, the mechanism will prompt incoming client connections to perform a number of successively more complex operations. The onion service will then prioritize these connections based on the effort level demonstrated by the client. We believe that the introduction of a proof-of-work mechanism will disincentivize attackers by making large-scale attacks costly and impractical while giving priority to legitimate traffic. Onion Services are encouraged to update to version [0.4.8.](https://forum.torproject.org/t/stable-release-0-4-8-4/8884)

## Why the need?

The inherent design of onion services, which prioritizes user privacy by obfuscating IP addresses, has made it vulnerable to [DoS attacks](<https://blog.torproject.org/tor-network-ddos-attack/>) and traditional IP-based rate limits have been imperfect protections in these scenarios. In need of alternative solutions, we devised a proof-of-work mechanism involving a client puzzle to thwart DoS attacks without compromising user privacy. 

## How does it work?

Proof of work acts as a ticket system that is turned off by default, but adapts to network stress by creating a priority queue. Before accessing an onion service, a small puzzle must be solved, proving that some "work" has been done by the client. The harder the puzzle, the more work is being performed, proving a user is genuine and not a bot trying to flood the service. Ultimately the proof-of-work mechanism blocks attackers while giving real users a chance to reach their destination.

## What does this mean for attackers and users?

If attackers attempt to flood an onion service with requests, the PoW defense will kick into action and increase the computational effort required to access a .onion site. This ticketing system aims to disadvantage attackers who make a huge number of connection attempts to an onion service. Sustaining these kinds of attacks will require a lot of computational effort on their part with diminishing returns, as the effort increases.

For everyday users, however, who tend to submit only a few requests at a time, the added computational effort of solving the puzzle is manageable for most devices, with initial times per solve ranging from 5 milliseconds for faster computers and up to 30 milliseconds for slower hardware. If the attack traffic increases, the effort of the work will increase, up to roughly 1 minute of work. While this process is invisible to the users and makes waiting on a proof-of-work solution comparable to waiting on a slow network connection, it has the distinct advantage of providing them with a chance to access the Tor network even when it is under stress by proving their humanity. 

## Where do we go from here?

Over the past year, we have put a lot of work into mitigating attacks on our network and enhancing our defense for onion services. The introduction of Tor's PoW defense not only positions onion services among the few communication protocols with built-in DoS protections but also, when adopted by major sites, promises to reduce the negative impact of targeted attacks on network speeds. The dynamic nature of this system helps balance the load during sudden surges in traffic ensuring more consistent and reliable access to onion services.