title: Empowering human rights defenders in Brazil, Ecuador & Mexico
---
author: duncan
---
pub_date: 2023-06-07
---
categories:
community
research
human rights
usability
global south
---
summary: As part of a new grant we've expanded our user feedback program to Brazil, Ecuador and Mexico in partnership with the Guardian Project and Tails.
---
body:

Back in 2017 we established the [Global South Strategy](https://blog.torproject.org/furthering-our-mission-in-the-global-south/) to further our mission of promoting human rights and internet freedom across the Global South. As part of this initiative, the Tor Project's [user feedback program was created](https://blog.torproject.org/reaching-people-where-they-are/) – which aims to improve the user experience of our products by conducting usability research with at-risk communities alongside digital security training.

In 2021, as part of a new grant, we expanded this program to Brazil, Ecuador and Mexico. We also partnered with two other organisations who, like the Tor Project, are committed to defending human rights and internet freedom with privacy preserving technology: the [Guardian Project](https://guardianproject.info/2023/06/02/improving-usability-of-tor-on-smartphones-in-latin-america/) and [Tails](https://tails.boum.org/news/improving_in_latam/). During the past two years we have [collaborately closely to combat internet censorship and surveillance](https://blog.torproject.org/tor-community-partners/), and are pleased to share some of our findings in this report.

![Flags of Brazil, Ecuador and Mexico](target-countries-banner-s60.png)

## Why Brazil, Ecuador and Mexico?

In response to the travel restrictions and limitations on in-person events imposed by the pandemic in 2021, we made a strategic decision to concentrate our efforts in three countries: Brazil, Ecuador, and Mexico.

**Brazil**: Since the presidential elections in 2018, threats and violence against minorities, indigenous communities, activists, feminists, journalists, and human rights defenders has increased.

**Ecuador**: During the pandemic, feminist collectives launched help lines to provide aid for their communities. Investigative journalists are often subject to [online and physical threats](https://twitter.com/SinCadenasECU/status/1637909814805164033).

**Mexico**: Journalists and human rights defenders are [frequent targets](https://nocallaronlasolas.serendipia.digital) of death threats, legal threats, arbitrary arrest, online and offline acts of intimidation, and [other forms of violence](https://r3d.mx/2023/05/11/el-periodista-mexicano-fredid-roman-fue-geolocalizado-en-secreto-un-dia-antes-de-su-asesinato/). Many public cases of [spyware and other malware being used](https://socialtic.org/blog/comunicado-nuevos-casos-de-espionaje-con-pegasus-en-mexico-no-deben-quedar-en-la-impunidad/) by different actors has emerged, including by the [Mexican army to spy on human rights defenders and journalists](https://socialtic.org/blog/ejercito-mexicano-espio-con-pegasus-a-dos-personas-defensoras-de-derechos-humanos-del-centro-prodh/).

Our activities in each country included two cycles of training workshops and usability testing sessions, held once per year. However due to the pandemic a significant portion of the activities belongning to the first cycle took place on online platforms like BigBlueButton and Jitsi (See the announcements for [Brazil and Mexico](https://forum.torproject.net/t/ciclo-de-autodefensa-digital-en-mexico-ciclo-de-autodefesa-digital-brasil/2124) and [Ecuador](https://forum.torproject.net/t/ecuador-talleres-de-anonimato-y-privacidad-en-ecuador-con-tor-project-tails-y-the-guardian-project/4273)). In-person activities then resumed from late 2022 until the end of the project in May 2023.

We were also joined by user researchers and trainers from the Guardian Project and Tails. Thanks to their participation, we were able to conduct more comprehensive user research within these communities and expand the tools we collected feedback on to include:

* [Onion Browser](https://onionbrowser.com/): a Tor-powered web browser for iOS created by Mike Tigas and maintained by the Guardian Project.
* [Orbot](https://orbot.app/): a popular Tor VPN available for Android and iOS, developed by the Guardian Project.
* [Tails](https://tails.boum.org/install/index.en.html): a Linux-based portable operating system powered by Tor that can be ran from a USB stick and leaves no trace on your computer.
* [Tor Browser](https://www.torproject.org/download/): the official web browser from the Tor Project for desktop and Android.

Over the course of this project, the Tor Project, The Guardian Project, and Tails have hosted 27 digital security training workshops in Brazil, Ecuador and Mexico. Through our combined efforts we managed to reach 47 human rights organizations, with a total of 674 journalists and human rights defenders trained.

> Through the specific knowledge given in the workshops, namely the exchange of information about existing tools and systems, the ways to access a bridge, the pluggable transports, and the snowflake extension, I will be able to keep myself safe and travel privately on the internet.
> 
> **— Workshop participant**

![Empathy, Respect, Consent](user-research-banner-s60.png)

## Conducting ethical user research

As part of our [shared commitment to respect the privacy of our users](https://blog.torproject.org/strength-numbers-usable-tools-dont-need-be-invasive/), none of these applications feature non-consensual feedback mechanisms like client-level tracking or analytics. Thus, opportunities like these where we can connect with at-risk communities are essential to the success of our human-centered design approach.
Participants from each country were recruited through a public call for participation hosted by Tails, and screened to prioritize human rights defenders, diversity, equity and inclusion.

![Project stats and gender breakdown](big-numbers-s60.png)

During these sessions we identified dozens of pain points experienced by our users, and used these findings to design and develop improvements to the user experience for each of the applications tested. As a direct result, Tor Browser will receive a new circuit display; clearer descriptions of censorship circumvention features; small refinements to connection settings; and a streamlined user flow when downloading files more in-keeping with modern Firefox.

For Orbot, Guardian Project have simplified the steps required to use Orbot as a VPN; refined the language used in the app to clarify Orbot's functionality; developed smart connect, which automatically selects the best connection method on the user's behalf; and have improved the user experience to select bridges manually.

Last but not least, Tails have restructured parts of their documentation in response to users having difficulty installing the operating system; made improvements to the Tor Connection assistant and bridge configuration in situations where Tor is blocked; and validated their new Persistent Storage feature – which, fortunately, did not require further action.

![The new circuit display in Tor Browser](tor-browser-improvements-s60.png)

In addition to the above product improvements, we consolidated much of what we learned from workshop participants into personas – a tool we use to foster empathy by helping developers understand the motivations, pain points, and goals of our users. From the Tor Project, meet [Rosa, the transfeminist campaigner](https://community.torproject.org/user-research/personas/) and [Daniel, the exiled journalist](https://community.torproject.org/user-research/personas/). From the Guardian Project, meet journalists and human rights defenders [Gregorio, Claudia, and Federica](https://guardianproject.info/2023/06/02/improving-usability-of-tor-on-smartphones-in-latin-america/GuardianProject-TorUsabilityOnSmartphonesLatinAmerica-May2023-v0.0.2.pdf).

Since the beginning of the pandemic many more organizers are working remotely, and rely on video conferencing services like Zoom or Jitsi to coordinate their activities. However training participants were discouraged to learn that these services won't work over Tor due to its lack of UDP support – a communication protocol that's essential to their functionality. Thankfully, UDP is [on the roadmap](https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/339-udp-over-tor.md) for [Arti](https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md), our project to rewrite Tor in the [Rust programming language](https://www.rust-lang.org/).

Many of our workshop participants reported that internet access continues to be expensive, slow and/or unreliable in each country. Those who cannot afford to constantly renew their data plans must deal with alternating internet access as a result. In such situations, using Tor-powered tools may require a degree of compromise between safety and convenience – emphasizing the importance of our work to [continue to improve the Tor network's overall performance](https://blog.torproject.org/tor-network-ddos-attack/).

It was also common knowledge among participants that Internet Service Providers and open wifi networks will monetize their browsing data, and many assumed other forms of monitoring may be happening as well. Despite this, participants found it incredibly difficult to differentiate short-term censorship events from other potential connectivity issues, and thus know when to switch to using a censorship circumvention tool to unblock the internet.

> I believe that understanding how these tools work allows us to adopt self-defense measures that are suited to our needs. Sometimes, we adopt tools without knowing how they work, and it can make us more vulnerable. So, I think it's fundamental to understand and learn to use them, and to know when to use them.
> 
> **— Workshop participant**

Given all of the above, supporting human rights defenders on the ground by providing in-depth training on threat modeling, how to identify censorship and use circumvention tools continues to be a necessary and invaluable aspect of our work in the Global South. What's more, by pairing digital security training with usability testing sessions, we've been able to establish a user feedback loop that ensures future product improvements are driven by the experiences of our most vulnerable users. Thank you to all the human rights defenders from Brazil, Ecuador and Mexico who participated in our research, and to our partners at the Guardian Project and Tails for all their hard work. Your contributions are what made this project possible.

## Further resources

### Workshop materials

* **Tor for Mobile Phones** – Guardian Project ([Español](https://docs.google.com/presentation/d/1xUbZJvn--Xqr0pumBPi9e9ByV5tIRfaK3uALI5t9hLg/edit#slide=id.g10357ee3516_2_57))
* **Onion Browser for iOS** – Guardian Project ([Español](https://docs.google.com/presentation/d/1ahd1ultFZt7b5NOD1dw6q1y8nEGb6uivOF7lpv_L8O0/edit#slide=id.p))
* **Anonymity and Privacy** – Guardian Project ([Español](https://docs.google.com/presentation/d/1UNHBWZ87x7SsA_WWr6Q50g24CePly2xf1ntDXVOmwwY/edit?usp=sharing))
* **Orbot for Mobile Devices** – Guardian Project ([Español](https://docs.google.com/presentation/d/1JHQEYL0GafDauBdT76VJXtdh0PY7ZbP2-cKBFLoMmuI/edit?usp=sharing))
* **Orbot Usability Cue Cards** – Guardian Project ([Spanish](https://drive.google.com/drive/u/1/folders/1BjAbxYkiNUT9TCrsKtRtKQCesYLUcZJu))
* **Your Secure Computer Anywhere** – Tails ([English](https://tails.boum.org/contribute/how/promote/material/slides/Ciclo_Autodefensa_Digital_202204/Tails-English.odp), [Español](https://tails.boum.org/contribute/how/promote/material/slides/Ciclo_Autodefensa_Digital_202204/Tails-Spanish.odp) & [Português](https://tails.boum.org/contribute/how/promote/material/slides/Ciclo_Autodefensa_Digital_202204/Tails-Portuguese.odp))
* **All About Tor** – Tor Project ([English](https://community.torproject.org/training/resources/), [Español](https://community.torproject.org/es/training/resources/))
* **Other Tor Training Resources** – Tor Project ([English](https://community.torproject.org/training/), [Español](https://community.torproject.org/es/training/))

### User personas

* **Rosa, the transfeminist campaigner** – Tor Project ([English](https://community.torproject.org/user-research/personas/))
* **Daniel, the exiled journalist** – Tor Project ([English](https://community.torproject.org/user-research/personas/))
* **Gregorio, Claudia, and Federica** – Guardian Project ([English](https://guardianproject.info/2023/06/02/improving-usability-of-tor-on-smartphones-in-latin-america/GuardianProject-TorUsabilityOnSmartphonesLatinAmerica-May2023-v0.0.2.pdf))

### Other public reports

* By the Guardian Project ([English](https://guardianproject.info/2023/06/02/improving-usability-of-tor-on-smartphones-in-latin-america/))
* By Tails ([English](https://tails.boum.org/news/improving_in_latam/))
