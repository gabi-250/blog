title: Tor Weekly News — April 29th, 2015
---
pub_date: 2015-04-29
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the seventeenth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor Browser 4.5 is out</h1>

<p>Mike Perry <a href="https://blog.torproject.org/blog/tor-browser-45-released" rel="nofollow">announced</a> the first stable release in Tor Browser’s 4.5 series. This version includes numerous major new features and updates, and represents a significant advance in user-friendly security technology.</p>

<p>The most visible new features have been covered in previous issues of Tor Weekly News. Tor Browser’s onion menu has been reorganized for ease of use, and now includes a diagram showing the locations and IP addresses of the relays that make up the Tor circuit used to access a website — one of the features most missed from the now-defunct Vidalia controller. The “security slider”, accessible in the onion menu’s “Privacy and Security Settings”, can be set at one of four levels depending on a user’s needs, disabling browser features which may give adversaries an opportunity to attack, at the cost of making some web pages less usable.</p>

<p>Tor Browser’s “first-party isolation” feature has been expanded: when you visit a website, all requests for the content on that domain name (including third-party elements like advertising beacons, analytics trackers, and content delivery networks) will be made over the same Tor circuit, and each domain name is restricted to its own Tor circuit, which is maintained for as long as the site is in active use. This makes an adversary’s tracking of a Tor user’s activity across different sites even harder than it was already, while ensuring that the usability of websites is not affected by sudden changes of exit relay.</p>

<p>For full explanations of these and other features — including better desktop integration, a new search provider, improvements to the software signature process, and more — please see the team’s announcement. Users of what was the 4.5-alpha series will be prompted to update automatically by their Tor Browser, while users of the stable 4.0.8 will receive the same prompt in about a week’s time, “because [the] changes are significant”. And if you don’t already have a working copy of Tor Browser, head to the <a href="https://www.torproject.org/projects/torbrowser.html" rel="nofollow">download page</a> to get started. Congratulations to the Tor Browser team on reaching this milestone!</p>

<h1>Miscellaneous news</h1>

<p>Karsten Loesing <a href="https://lists.torproject.org/pipermail/onionoo-announce/2015/000004.html" rel="nofollow">announced</a> that the onionoo-announce mailing list will be shut down in favor of posting announcements about major Onionoo protocol updates to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-dev" rel="nofollow">tor-dev</a> mailing list, “because each Tor mailing list makes it more difficult for new contributors to decide which of them to subscribe to”. If these announcements are relevant to your work, please be sure to subscribe to tor-dev — you can set your mail client to filter for the keyword “Onionoo” if you’d rather not receive other Tor development-related messages.</p>

<p>Also in Onionoo news, Thomas White <a href="https://lists.torproject.org/pipermail/tor-talk/2015-April/037619.html" rel="nofollow">announced</a> that his mirror of the service is now also available at an onion address.</p>

<p>Lucas Erlacher <a href="https://lists.torproject.org/pipermail/tor-dev/2015-April/008755.html" rel="nofollow">announced</a> version 0.3.0 of OnionPy, “a pure-python Onionoo request wrapper that supports transparent caching”. The new release respects Onionoo’s “version” field in query responses.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

