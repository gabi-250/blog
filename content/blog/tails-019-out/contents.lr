title: Tails 0.19 is out
---
pub_date: 2013-06-25
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 0.19, is out.</p>

<p>All users must upgrade as soon as possible.</p>

<p><a href="https://tails.boum.org/download/" rel="nofollow">Download it now.</a></p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li>New features
<ul>
<li>Linux 3.9.5-1.</li>
<li>Iceweasel 17.0.7esr + Torbrowser patches.</li>
<li>Unblock Bluetooth, Wi-Fi, WWAN and WiMAX; block every other type of wireless device.</li>
</ul>
</li>
<li>Bugfixes
<ul>
<li>Fix write access to boot medium at the block device level.</li>
<li>tails-greeter l10n-related fixes.</li>
<li>gpgApplet: partial fix for clipboard emptying after a wrong passphrase was entered.</li>
</ul>
</li>
<li>Minor improvements
<ul>
<li>Drop GNOME proxy settings.</li>
<li>Format newly created persistent volumes as ext4.</li>
<li>GnuPG: don't connect to the keyserver specified by the key owner.</li>
<li>GnuPG: locate keys only from local keyrings.</li>
<li>Upgrade live-boot and live-config to the 3.0.x final version from Wheezy.</li>
</ul>
</li>
<li>Localization: many translation updates all over the place.</li>
<li>Test suite
<ul>
<li>Re-enable previously disabled boot device permissions test.</li>
</ul>
</li>
</ul>

<p>See the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog?id=0.19" rel="nofollow">online Changelog</a> for technical details.</p>

<p><strong>Known issues</strong></p>

<p>No new known issue but <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">longstanding known issues</a>.</p>

<p><strong>I want to try it / to upgrade!</strong></p>

<p>See the <a href="https://tails.boum.org/getting_started/" rel="nofollow">Getting started</a> page.</p>

<p>As no software is ever perfect, we maintain a list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">problems that affects the last release of Tails</a>.</p>

<p><strong>What's coming up?</strong></p>

<p>The next Tails release is scheduled for early August.</p>

<p>Have a look to our <a href="https://tails.boum.org/contribute/roadmap/" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Would you want to help? As explained in our <a href="https://tails.boum.org/contribute/" rel="nofollow">"how to contribute" documentation</a>, there are many ways <strong>you</strong> can contribute to Tails. If you want to help, come talk to us!</p>

