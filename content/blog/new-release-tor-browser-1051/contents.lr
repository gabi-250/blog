title: New Release: Tor Browser 10.5.1 (Android Only)
---
pub_date: 2021-07-07
---
author: sysrqb
---
tags:

tor browser
tbb-10.5
tbb
---
categories: applications
---
summary: Tor Browser 10.5.1 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 10.5.1 is now available from the <a href="https://www.torproject.org/download/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5.1/">distribution directory</a>.</p>
<p>This version is a bugfix for Android Tor Browser 10.5.</p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser will <em><b>stop</b> supporting <u>version 2 onion services</u></em> later <em><b>this year</b></em>. Please see the previously published <a href="https://blog.torproject.org/v2-deprecation-timeline">deprecation timeline</a>. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.5">Tor Browser 10.5</a>:</p>
<ul>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40324">Bug 40324</a>: Change Fenix variant to Release</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292192"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292192" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 08, 2021</p>
    </div>
    <a href="#comment-292192">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292192" class="permalink" rel="bookmark">Snowflake isn&#039;t available in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Snowflake isn't available in 10.5.1.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292302"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292302" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 13, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292192" class="permalink" rel="bookmark">Snowflake isn&#039;t available in…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292302">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292302" class="permalink" rel="bookmark">Yes, it will be available in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, it will be available in 10.5.3.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292193"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292193" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 08, 2021</p>
    </div>
    <a href="#comment-292193">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292193" class="permalink" rel="bookmark">I know this was a small…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I know this was a small mistake that was overlooked, but what's the difference between the Fenix variant being Beta vs Release? Out of curiosity.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292218"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292218" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Russian man (not verified)</span> said:</p>
      <p class="date-time">July 09, 2021</p>
    </div>
    <a href="#comment-292218">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292218" class="permalink" rel="bookmark">Browser super!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Browser super!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292231"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292231" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 09, 2021</p>
    </div>
    <a href="#comment-292231">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292231" class="permalink" rel="bookmark">https://bugs.torproject.org…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://bugs.torproject.org/tpo/applications/fenix/40324" rel="nofollow">https://bugs.torproject.org/tpo/applications/fenix/40324</a><br />
Bug 40324: Change Fenix variant to Release</p>
<p>Click:<br />
404 Page Not Found</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292301"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292301" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 13, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292231" class="permalink" rel="bookmark">https://bugs.torproject.org…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292301">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292301" class="permalink" rel="bookmark">Thanks, fixed. That should…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks, fixed. That should be <a href="https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40324" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issu…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292317"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292317" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Someone (not verified)</span> said:</p>
      <p class="date-time">July 13, 2021</p>
    </div>
    <a href="#comment-292317">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292317" class="permalink" rel="bookmark">Thank you for the update!
…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for the update!</p>
<p>Will this be available through F-Droid or do we need to download and update manually? As of today it still hasn't showed up there. Not sure if we need to ask here or to Guardian Project...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292404"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292404" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292317" class="permalink" rel="bookmark">Thank you for the update!
…</a> by <span>Someone (not verified)</span></p>
    <a href="#comment-292404">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292404" class="permalink" rel="bookmark">Thanks, I&#039;ll investigate.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks, I'll investigate.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292352"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292352" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 16, 2021</p>
    </div>
    <a href="#comment-292352">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292352" class="permalink" rel="bookmark">Attention Android users: 
…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Attention Android users: </p>
<p>If you experience regular or repeated crashes, please install <a href="https://f-droid.org/en/packages/taco.scoop/" rel="nofollow">Scoop</a> from f-droid. Scoop monitors app crashes and captures a stack trace, which contains information that can be extremely helpful for developers. If Scoop detects a crash, please file a <a href="https://support.torproject.org/misc/bug-or-feedback/" rel="nofollow">bug report</a> and include a copy of the stack trace with it.</p>
<p>As per the <a href="https://web.archive.org/web/20210427172207/https://github.com/TacoTheDank/Scoop/wiki" rel="nofollow">instructions</a>, non-root users must run the following command on the device in order to enable Scoop. You do this from a desktop using the <span class="geshifilter"><code class="php geshifilter-php">adb shell</code></span> command, or from the device itself using a terminal emulator app such as <a href="https://f-droid.org/en/packages/com.termux/" rel="nofollow">Termux</a> or <a href="https://search.f-droid.org/?q=terminal" rel="nofollow">many others</a>. Neither method requires root.</p>
<p><span class="geshifilter"><code class="php geshifilter-php">pm grant taco<span style="color: #339933;">.</span>scoop android<span style="color: #339933;">.</span>permission<span style="color: #339933;">.</span>READ_LOGS</code></span></p>
<p>(See <a href="https://gitlab.torproject.org/tpo/web/support/-/issues/209" rel="nofollow">https://gitlab.torproject.org/tpo/web/support/-/issues/209</a>)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292374"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292374" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>P (not verified)</span> said:</p>
      <p class="date-time">July 17, 2021</p>
    </div>
    <a href="#comment-292374">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292374" class="permalink" rel="bookmark">Leaks keyboard language</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Leaks keyboard language</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292403"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292403" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292374" class="permalink" rel="bookmark">Leaks keyboard language</a> by <span>P (not verified)</span></p>
    <a href="#comment-292403">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292403" class="permalink" rel="bookmark">Yes, this is a difficult…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, this is a difficult issue to fix: <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/16678" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/166…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292382"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292382" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Linda Sprague (not verified)</span> said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
    <a href="#comment-292382">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292382" class="permalink" rel="bookmark">Will this work on the phone…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Will this work on the phone Android 9?   Some of the apps are version 10. I don't know why they all aren't.<br />
then if I install this browser do I uninstall my other browser Chrome and Google?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292402"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292402" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292382" class="permalink" rel="bookmark">Will this work on the phone…</a> by <span>Linda Sprague (not verified)</span></p>
    <a href="#comment-292402">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292402" class="permalink" rel="bookmark">Yes, this browser should…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, this browser should work on Android 9. Tor Browser is supported on the same Android versions as Mozilla Firefox: <a href="https://support.mozilla.org/en-US/kb/will-firefox-work-my-mobile-device" rel="nofollow">https://support.mozilla.org/en-US/kb/will-firefox-work-my-mobile-device</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
