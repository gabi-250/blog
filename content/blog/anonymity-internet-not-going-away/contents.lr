title: Anonymity on the Internet is not going away.
---
pub_date: 2008-11-07
---
author: phobos
---
_html_body:

<p>A few people have told me about <a href="http://www.techradar.com/news/computing/will-the-internet-always-be-anonymous--482519" rel="nofollow">this TechRadar</a> story.  The implication is that the US Air Force is going to <a href="https://www.fbo.gov/index?print_preview=1&amp;s=opportunity&amp;mode=form&amp;id=e72854d6e3c1a044038563ef1e0fdfa6&amp;tab=core&amp;tabmode=list&amp;cck=1&amp;au=&amp;ck=" rel="nofollow">do away</a> with the anonymity of the Internet.  In reality, I think these people are looking for Tor's opinion on this sort of "news".  Rather than pick apart the silliness of the statements, a few things bothered me about the TechRadar article; which I've already heard many times.</p>

<p>Let's tackle the misnomer that the Internet is inherently anonymous.  This quote states the "common wisdom":</p>

<blockquote><p>It's true that the TCP/IP protocol, as currently implemented, makes it very hard to verify the source of any given network packet, but that's purely because the network architects chose to make it that way.</p></blockquote>

<p>In fact, IP addresses are designed and used for routing.  It's easy to figure out where traffic originates if you have the IP address.  What's not so easy is figuring out if the owner of the system at that IP address was the actual sender of the traffic.  There's a popular notion that you are your IP address; and that actions taken with your IP can be tied back to you.  IP addresses are for routing, not authentication.  The Air Force plan wants to solve the latter problem of authentication (Network friend or foe).  A subpoena or legal demand of a provider (blog, forum, ISP, etc) can reveal the IP address and possibly its owner.</p>

<p>The larger concern with the article is that it states:</p>

<blockquote><p>But it's also what lets protesters protest and dissidents diss, so there are some genuinely valid reasons for wanting to preserve internet privacy</p></blockquote>

<p>and then ends with</p>

<blockquote><p>Anonymous internets will always exist - the terrorists, the paedophiles and the tin-foil-hat brigade will make sure of that. But in 10 years time, the idea of the mainstream internet - the one that all of us use every day - being anonymous, will seem as quaint as a street without CCTV cameras.</p></blockquote>

<p>Anonymity is a defense against the tyranny of the majority.  There are <a href="https://www.torproject.org/torusers" rel="nofollow">many, many valid uses</a> of anonymity tools, such as Tor.  The belief that anonymous tools exist only for the edges of societies is narrow-minded.  The tools exist and are used by all.  Much like the Internet, the tools can be used for good or bad.  The negative uses of such tools typically generate huge headlines, but not the positive uses.  Raising the profile of the positive uses of anonymity tools, such as Tor, is one of our challenges.</p>

---
_comments:

<a id="comment-440"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-440" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 18, 2008</p>
    </div>
    <a href="#comment-440">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-440" class="permalink" rel="bookmark">&quot;Quaint&quot; privacy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The idea of streets without CCTV is not nor will be quaint to most of the world; it's a reality which the Germans of my acquaintance tease me over regularly, since my nation (UK) is widely reported as the most monitored (by CCTV) nation in the world and in their's it remains mostly unused, especially in public areas.</p>
</div>
  </div>
</article>
<!-- Comment END -->
