title: Tor Weekly News — November 20th, 2013
---
pub_date: 2013-11-20
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twenty-first issue of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news/" rel="nofollow">weekly newsletter that covers what is happening in the Tor community.</a></p>

<h1>tor 0.2.4.18-rc is out</h1>

<p>On the 16th of November, Roger Dingledine <a href="https://lists.torproject.org/pipermail/tor-talk/2013-November/031110.html" rel="nofollow">released the fourth release candidate for the tor 0.2.4.x series </a>. As Roger puts it: “It takes a variety of fixes from the 0.2.5.x branch to improve stability, performance, and better handling of edge cases.” Readers curious for more details can look at the announcement for the complete list of changes.</p>

<p>The <a href="https://www.torproject.org/dist/" rel="nofollow">source is available</a> as well as <a href="https://www.torproject.org/docs/debian.html#development" rel="nofollow">updated Debian packages</a>. All relay operators should upgrade. Updated Tor Browser Bundles are in the making and should be available shortly.</p>

<h1>USB Sticks for Tails</h1>

<p>It is often recommended to run Tails from a read-only medium in order to prevent any malware to permanently mess with the system. “CD is best, but many devices these days don’t have an optical drive, and handling CDs is not as convenient as a USB stick” <a href="https://lists.torproject.org/pipermail/tor-talk/2013-November/031092.html" rel="nofollow">wrote Moritz Bartl on tor-talk</a>.</p>

<p>It looks like one of the very few specific brand of USB sticks available in Germany that had a proper hardware protection switch can no longer be used to boot Tails. Moritz ended up contacting various Chinese suppliers. “Even there, the selection of sticks with write protection is very limited” but eventually one model was found acceptable. Moritz intend to re-sell a bulk of them at the <a href="https://events.ccc.de/congress/2013/" rel="nofollow">upcoming 30C3 in Hamburg</a>.</p>

<p>Feel free to join the discussion or contact Moritz privately for more details.</p>

<h1>New version of check.torproject.org</h1>

<p>On the 15th of November, regular users of the Tor Browser Bundle have probably noticed a change in their preferred welcome page. Andrew Lewman had just <a href="https://lists.torproject.org/pipermail/tor-talk/2013-November/031126.html" rel="nofollow">switched check.torproject.org to a new version</a> <a href="https://bugs.torproject.org/9529" rel="nofollow">written by Arlo Breault in Go</a>. The <a href="https://gitweb.torproject.org/check.git" rel="nofollow">new codebase</a> should allow the service to better handle the increasingly high number of connections. <a href="https://trac.torproject.org/projects/tor/query?status=closed&amp;changetime=2013-11-15..&amp;component=Tor+Check" rel="nofollow">Several fixes</a> were also made during the reimplementation regarding wording, translations and other meaningful details.</p>

<p>Please report any issues you encounter to the “Tor Check” component of the Tor bug tracker.</p>

<h1>Current state of the proposals</h1>

<p>In 2007, Tor developers settled on a <a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/001-process.txt" rel="nofollow">formal process</a> for changes in Tor specifications or other major changes. At this heart of this process in the “proposal” documents that are discussed on the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-dev" rel="nofollow">tor-dev mailing list</a> and archived in the <a href="https://gitweb.torproject.org/torspec.git/tree/HEAD:/proposals" rel="nofollow">“torspec” Git repository</a>.</p>

<p>Last week, Nick Mathewson took a closer look at what have changed since the <a href="https://lists.torproject.org/pipermail/tor-dev/2012-June/003641.html" rel="nofollow">last round up</a> he did in June last year. <a href="https://lists.torproject.org/pipermail/tor-dev/2013-November/005797.html" rel="nofollow">Since then</a>, 16 proposals has been implemented in tor 0.2.3, 0.2.4 and 0.2.5 and two have been superseded or deemed unhelpful.</p>

<p>Nick subsequently <a href="https://lists.torproject.org/pipermail/tor-dev/2013-November/005798.html" rel="nofollow">posted</a> a review of all “open”, “needs-revision”, and “needs-research” proposals. They are many different tasks to be picked by someone who wishes to help Tor in these 42 proposals, be it by doing research, code, leading discussions or more in-depth analysis.</p>

<h1>Miscellaneous news</h1>

<p>Radu Rădeanu came up with a <a href="https://bugs.torproject.org/9353#comment:38" rel="nofollow">workaround</a> for Tor users on Ubuntu 13.10 which temporarily fixes keyboard bug in 64-bit Tor Browser Bundles when used in combination with IBus.</p>

<p>Roger Dingledine <a href="https://lists.torproject.org/pipermail/tor-talk/2013-November/031134.html" rel="nofollow">called for help</a> in the <a href="https://bugs.torproject.org/10182" rel="nofollow">collection</a> of the new Tor related articles in the press. “According to <a href="https://www.torproject.org/press/press" rel="nofollow">the website</a> the last time there was a meaningful article about Tor was July 1st. This is very far from the case.” If you want to help, just <a href="https://trac.torproject.org/projects/tor/wiki/TorArticles" rel="nofollow">edit the wiki page</a>.</p>

<p>Firefox 24 is soon going to replace version 17 as “stable” supported release by Mozilla. intrigeri has <a href="https://mailman.boum.org/pipermail/tails-dev/2013-November/004165.html" rel="nofollow">completed his work</a> in updating Tails’ browser to the point where it “is good enough for Tails 0.22”. Builds from the “feature/ff24” are available <a href="http://nightly.tails.boum.org/build_Tails_ISO_feature-ff24/" rel="nofollow"></a> for wider testing.</p>

<p>Andreas Jonsson <a href="https://lists.torproject.org/pipermail/tor-talk/2013-November/031111.html" rel="nofollow">released</a> initial sandboxed version of the TBB 3.0 series which is <a href="https://github.com/trams242/tor-browser-bundle" rel="nofollow">ready for testing</a>. This security feature should prevent an exploit from stealing user data : the Tor Browser will not be allowed to execute any programs, nor will it be allowed to read or modify data on disk except in the users “downloads”-folder and its own profile. The sandbox is currently only supported on OS X 10.9 “but making it work all the way down to 10.6 is not unlikely”.</p>

<p>Check Mike Perry’s <a href="https://lists.torproject.org/pipermail/tor-reports/2013-November/000384.html" rel="nofollow">latest report</a> to see what he has been up to in October.</p>

<h1>Tor help desk roundup</h1>

<p>Users have asked the help desk for support connecting to IRC through Tor. There are some guides on sending IRC traffic through Tor on the wiki (<a href="https://trac.torproject.org/projects/tor/wiki/doc/TorifyHOWTO/InstantMessaging" rel="nofollow">1</a>, <a href="https://trac.torproject.org/projects/tor/wiki/doc/TorifyHOWTO/IrcSilc" rel="nofollow">2</a>). Tails also comes with <a href="https://tails.boum.org/doc/anonymous_internet/pidgin/" rel="nofollow">Pidgin preconfigured for IRC</a>. However, it will not matter if the IRC client is correctly configured if the if the intended IRC network blocks all Tor users. For example, users trying to connect to synIRC through Tor will receive a message telling them their computer is part of a botnet.</p>

<p>Users will occasionally ask for support using Tor on ChromeOS. ChromeOS is based on Linux, so it is theoretically possible to run the Linux Tor Browser Bundle on ChromeOS. In practice, the Chromebook prevents users from executing new software on their computers without putting their Chromebook into developer mode, and <a href="http://www.chromium.org/chromium-os/chromiumos-design-docs/security-overview" rel="nofollow">making other modifications to their device</a>. Anyone who has successfully run the Tor Browser Bundle on ChromeOS is invited to describe their experiences on the Tor Project wiki. As of this writing, there is no documented way of running Tor Browser Bundle on ChromeOS.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, dope457, Matt Pagan, and Andreas Jonsson.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

