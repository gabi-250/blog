title: New Release: Tor Browser 10.5a3
---
pub_date: 2020-11-13
---
author: sysrqb
---
tags:

tbb-10.5
tbb
tor browser
---
categories: applications
---
_html_body:

<p>Tor Browser 10.5a3 for Desktop platforms is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5a3/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-1004">latest stable release</a> instead.</p>
<p>Tor Browser 10.5a3 updates NoScript to 11.1.5 and libevent to 2.1.12. This release includes important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2020-49/">security updates</a> to Firefox.</p>
<p><strong>Note:</strong> Tor Browser 10.5 <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40089">does not support CentOS 6</a>.</p>
<p>The <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">full changelog</a> since Tor Browser 10.5a2 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Update NoScript to 11.1.5</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40022">Bug 40022</a>: EOY November Update - Matching</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40064">Bug 40064</a>: Bump libevent to 2.1.12</li>
<li>Translations update</li>
</ul>
</li>
<li>Windows + OS X + Linux
<ul>
<li><a href="https://bugs.torproject.org/27002">Bug 27002</a>: (Mozilla 1673237) Always allow SVGs on about: pages</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40021">Bug 40021</a>: Keep page shown after Tor Browser update purple</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40137">Bug 40137</a>: Migrate https-everywhere storage to idb</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40219">Bug 40219</a>: Backport fix for Mozilla's bug 1675905</li>
</ul>
</li>
<li>Android
<ul>
<li>Pick up fix for Mozilla's bug 1675905 (with GeckoView 82.0.3)</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40106">Bug 40106</a>: EOY November Update - Matching</li>
</ul>
</li>
<li>Build System
<ul>
<li>All Platforms
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40079">Bug 40079</a>: Bump Go to 1.15.4</li>
</ul>
</li>
<li>Windows + OS X + Linux
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40133">Bug 40133</a>: Bump Rust version for ESR 78 to 1.43.0</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-290378"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290378" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 14, 2020</p>
    </div>
    <a href="#comment-290378">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290378" class="permalink" rel="bookmark">&gt; Bug 40137: Migrate https…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Bug 40137: Migrate https-everywhere storage to idb<br />
I didn't see anything about migration in the logs after the update...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290548"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290548" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">November 28, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290378" class="permalink" rel="bookmark">&gt; Bug 40137: Migrate https…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-290548">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290548" class="permalink" rel="bookmark">Did the migration already…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Did the migration already occur in a previous update? If the https-everywhere data already migrated then it wouldn't migrate again.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-290382"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290382" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 14, 2020</p>
    </div>
    <a href="#comment-290382">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290382" class="permalink" rel="bookmark">I want to help the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I want to help the development of Tor Browser but I don't know whether I should test alpha versions or not. Will the browser's fingerprint be different?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290549"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290549" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">November 28, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290382" class="permalink" rel="bookmark">I want to help the…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-290549">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290549" class="permalink" rel="bookmark">The fingerprint may be…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The fingerprint may be different, and the difference changes over time as new features/functionality is tested in alpha versions.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-290384"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290384" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 14, 2020</p>
    </div>
    <a href="#comment-290384">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290384" class="permalink" rel="bookmark">Bug 40137: Migrate https…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Bug 40137: Migrate https-everywhere storage to idb<br />
<a href="https://bugs.torproject.org/tpo/applications/tor-browser/40137" rel="nofollow">https://bugs.torproject.org/tpo/applications/tor-browser/40137</a></p>
<p>/profile.default/storage/ still deletable after browserexit or https-everywhere, the browser, is going nuts?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290550"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290550" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">November 28, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290384" class="permalink" rel="bookmark">Bug 40137: Migrate https…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-290550">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290550" class="permalink" rel="bookmark">I don&#039;t understand this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't understand this question, can you clarify?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
