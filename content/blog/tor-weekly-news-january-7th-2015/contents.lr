title: Tor Weekly News — January 7th, 2015
---
pub_date: 2015-01-07
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the first issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor 0.2.6.2-alpha is out</h1>

<p>Nick Mathewson <a href="https://blog.torproject.org/blog/tor-0262-alpha-released" rel="nofollow">announced</a> the second alpha release in the Tor 0.2.6.x series. As well as including the <a href="https://bugs.torproject.org/9262" rel="nofollow">cell scheduling changes</a> and <a href="https://bugs.torproject.org/13192" rel="nofollow">hidden service statistics collection</a> reported in recent issues of TWN, this release makes it harder to portscan hidden services by closing circuits if a client tries to connect to a non-existent port. It also contains numerous bugfixes and new unit tests; please see Nick’s announcement for the full changelog. The source code is available as usual from the <a href="https://dist.torproject.org/" rel="nofollow">distribution directory</a>.</p>

<h1>Tor at 31c3</h1>

<p>The 31st edition of the <a href="https://events.ccc.de/congress/2014/wiki/Main_Page" rel="nofollow">Chaos Communication Congress</a>, an annual highlight in the free software and security calendar, took place in Hamburg, and as usual Tor featured in several key talks over the course of the long weekend.</p>

<p>Roger Dingledine and Jacob Appelbaum’s appropriately grand-sounding <a href="http://media.ccc.de/browse/congress/2014/31c3_-_6251_-_en_-_saal_1_-_201412301400_-_state_of_the_onion_-_jacob_-_arma.html" rel="nofollow">“State of the Onion” address</a>, a round-up of the year’s events in the Tor community, took place once again, with guest contributions from journalist and filmmaker Laura Poitras and OONI developer Arturo Filastò. Topics included the relationship between censorship and surveillance, the misinterpretation of academic research by journalists, new pluggable transports, and much more.</p>

<p>Laura Poitras also joined Julia Angwin, Jack Gillum, and Nadia Heninger for <a href="http://media.ccc.de/browse/congress/2014/31c3_-_6154_-_en_-_saal_1_-_201412272300_-_crypto_tales_from_the_trenches_-_nadia_heninger_-_julia_angwin_-_laura_poitras_-_jack_gillum.html" rel="nofollow">“Crypto Tales from the Trenches”</a>, in which the journalists described their experiences with security software when doing research  and communicating with sources. “I don’t think any of us could do our work without Tor”, said Laura, while Julia described the Tails operating system as “her favorite success story” in this field.</p>

<p>Tor Browser developer Mike Perry joined Seth Schoen to <a href="http://media.ccc.de/browse/congress/2014/31c3_-_6240_-_en_-_saal_g_-_201412271400_-_reproducible_builds_-_mike_perry_-_seth_schoen_-_hans_steiner.html" rel="nofollow">discuss</a> the concept of deterministic builds, the implementation of which has been one of the Tor Project’s major successes over the past year. Mike and Seth demonstrated some of the attacks that this system aims to defend against, as well as the work that Tor, F-Droid, and Debian have all been doing to make their processes compatible with the deterministic build process.</p>

<p>Finally, Dr. Gareth Owen of Portsmouth University <a href="http://media.ccc.de/browse/congress/2014/31c3_-_6112_-_en_-_saal_2_-_201412301715_-_tor_hidden_services_and_deanonymisation_-_dr_gareth_owen.html" rel="nofollow">presented</a> the results of research into the content and usage of Tor hidden services. The research involved setting up a number of Tor relays, waiting until they gained the “HSDir” flag, then counting the number of times a particular service’s descriptor was requested, as well as manually categorizing the services whose descriptors were learned. Dr.  Owen found that while the largest category of onion services by number could be characterized as “drugs”, the majority of the descriptor requests he saw were for services in his “abuse” category. The talk itself discusses some possible limitations of the data gathered, and Tor developers have responded on the Tor blog with <a href="https://blog.torproject.org/blog/tor-80-percent-percent-1-2-percent-abusive" rel="nofollow">further</a> <a href="https://blog.torproject.org/blog/some-thoughts-hidden-services" rel="nofollow">analysis</a>.</p>

<h1>Monthly status reports for December 2014</h1>

<p>The wave of regular monthly reports from Tor project members for the month of December has begun. <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000727.html" rel="nofollow">Philipp Winter</a> released his report first, followed by reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000728.html" rel="nofollow">Damian Johnson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000729.html" rel="nofollow">Pearl Crescent</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000730.html" rel="nofollow">Juha Nurmi</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000731.html" rel="nofollow">Nick Mathewson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-December/000732.html" rel="nofollow">Sherief Alaa</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-January/000733.html" rel="nofollow">Sukhbir Singh</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-January/000734.html" rel="nofollow">Leiah Jansen</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-January/000735.html" rel="nofollow">David Goulet</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-January/000736.html" rel="nofollow">Michael Schloh von Bennewitz</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-January/000738.html" rel="nofollow">Colin C.</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-January/000740.html" rel="nofollow">Georg Koppen</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-January/000742.html" rel="nofollow">Arlo Breault</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2015-January/000743.html" rel="nofollow">George Kadianakis</a>.</p>

<p>Colin C. also sent out the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-January/000737.html" rel="nofollow">help desk report</a>, while Arturo Filastò reported on behalf of the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-January/000739.html" rel="nofollow">OONI team</a> and Mike Perry for the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-January/000741.html" rel="nofollow">Tor Browser team</a>.</p>

<h1>Miscellaneous news</h1>

<p>Nick Mathewson and Andrea Shepard drafted a <a href="https://lists.torproject.org/pipermail/tor-dev/2015-January/008087.html" rel="nofollow">proposal</a> for including a hash chain in the <a href="https://metrics.torproject.org/about.html#consensus" rel="nofollow">consensus</a> produced by Tor <a href="https://metrics.torproject.org/about.html#directory-authority" rel="nofollow">directory authorities</a>, in order to prevent certain kinds of attack on the directory authorities and their keys.</p>

<p>Nick also <a href="https://lists.torproject.org/pipermail/tor-talk/2015-January/036379.html" rel="nofollow">clarified</a> that a recently-discovered Libevent vulnerability has no effect on Tor.</p>

<p>In connection with the current push to collect statistics relating to Tor hidden services in a privacy-preserving manner, Aaron Johnson <a href="https://lists.torproject.org/pipermail/tor-dev/2015-January/008086.html" rel="nofollow">noted</a> that there are two further desirable sets of statistics which might pose a risk to anonymity if gathered incorrectly, and discussed possible solutions to the problem.</p>

<p>David Fifield published a <a href="https://lists.torproject.org/pipermail/tor-dev/2015-January/008082.html" rel="nofollow">summary</a> of costs incurred by the meek pluggable transport for the month of December 2014.</p>

<p>David also continued his experiments on historical Tor metrics data with <a href="https://lists.torproject.org/pipermail/tor-dev/2015-January/008095.html" rel="nofollow">visualizations of a recent Sybil attack</a>, and <a href="https://lists.torproject.org/pipermail/tor-talk/2015-January/036346.html" rel="nofollow">wondered</a> what might have been responsible for a sudden change in the way that users in Kazakhstan were choosing to connect to the Tor network in October 2014.</p>

<p>Sebastian Urbach <a href="https://lists.torproject.org/pipermail/tor-relays/2015-January/006051.html" rel="nofollow">noted</a> a sudden drop in the number of Tor relays acting as hidden service directories, and wondered about the cause. As SiNA Rabbani <a href="https://lists.torproject.org/pipermail/tor-relays/2015-January/006063.html" rel="nofollow">clarified</a>, the amount of time a relay needs to have been running before it earns the “HSDir” flag was increased by directory authorities, in response to a recent Sybil attack.</p>

<p>The developers of ChatSecure for iOS <a href="https://chatsecure.org/blog/chatsecure-ios-v3-released/" rel="nofollow">announced</a> that their recent 3.0 release includes experimental support for connections to XMPP chat servers over Tor, and briefly described how they added the new feature.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony, David Fifield, Catfish, and Karsten Loesing.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

