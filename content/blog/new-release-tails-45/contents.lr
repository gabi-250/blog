title: New Release: Tails 4.5
---
pub_date: 2020-04-07
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>The Tails team is happy to publish Tails 4.5, the first version of Tails to support Secure Boot.<br />
This release also fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_4.4.1/" rel="nofollow">many security vulnerabilities</a>. You should upgrade as soon as possible.</p>

<h1>New features</h1>

<h2>Secure Boot</h2>

<p>Tails now starts on computers with Secure Boot enabled.<br />
If your Mac displays the following error:<br />
Security settings do not allow this Mac to use an external startup disk.<br />
Then you have to <a href="https://tails.boum.org/install/mac/usb/#startup-security" rel="nofollow">change the settings of the <em>Startup Security Utility</em></a> of your Mac to authorize starting from Tails.</p>

<h1>Changes and updates</h1>

<ul>
<li>
Update <em>Tor Browser</em> to <a href="https://blog.torproject.org/new-release-tor-browser-909" rel="nofollow">9.0.9</a>.<br />
This update fixes several <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2020%2D13/" rel="nofollow">vulnerabilities</a> in <em>Firefox</em>, including <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2020%2D11/" rel="nofollow">some critical ones</a>.<br />
Mozilla is aware of targeted attacks in the wild abusing these vulnerabilities.
</li>
</ul>

<h1>Known issues</h1>

<p>None specific to this release.<br />
See the list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">long-standing issues</a>.</p>

<h1><a rel="nofollow">Get Tails 4.5</a></h1>

<h2><a rel="nofollow">To upgrade your Tails USB stick and keep your persistent storage</a></h2>

<ul>
<li>
Automatic upgrades are available from Tails 4.2 or later to 4.5.
</li>
<li>
If you cannot do an automatic upgrade or if Tails fails to start after an automatic upgrade, please try to do a <a href="https://tails.boum.org/doc/upgrade/#manual" rel="nofollow">manual upgrade</a>.
</li>
</ul>

<h2><a rel="nofollow">To install Tails on a new USB stick</a></h2>

<p>Follow our installation instructions:</p>

<ul>
<li><a href="https://tails.boum.org/install/win/" rel="nofollow">Install from Windows</a></li>
<li><a href="https://tails.boum.org/install/mac/" rel="nofollow">Install from macOS</a></li>
<li><a href="https://tails.boum.org/install/linux/" rel="nofollow">Install from Linux</a></li>
</ul>

<p>All the data on this USB stick will be lost.</p>

<h2><a rel="nofollow">To download only</a></h2>

<p>If you don't need installation or upgrade instructions, you can download Tails 4.5 directly:</p>

<ul>
<li><a href="https://tails.boum.org/install/download/" rel="nofollow">For USB sticks (USB image)</a></li>
<li><a href="https://tails.boum.org/install/download-iso/" rel="nofollow">For DVDs and virtual machines (ISO image)</a></li>
</ul>

<h1><a rel="nofollow">What's coming up?</a></h1>

<p>Tails 4.6 is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for May 5.<br />
Have a look at our <a href="https://tails.boum.org/contribute/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.<br />
We need your help and there are many ways to <a href="https://tails.boum.org/contribute/" rel="nofollow">contribute to Tails</a> (<a href="https://tails.boum.org/donate/?r=4.5" rel="nofollow">donating</a> is only one of them). Come <a href="https://tails.boum.org/about/contact/#tails-dev" rel="nofollow">talk to us</a>!</p>

<h1>Support and feedback</h1>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

