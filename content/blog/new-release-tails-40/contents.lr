title: New Release: Tails 4.0
---
pub_date: 2019-10-22
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>The Tails team is especially proud to present you Tails 4.0, the first version of Tails based on Debian 10 (Buster). It brings new versions of most of the software included in Tails and some important usability and performance improvements. Tails 4.0 introduces more changes than any other version since years.<br />
This release also fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_3.16/" rel="nofollow">many security issues</a>. You should upgrade as soon as possible.</p>

<h1>Changes and upgrades</h1>

<h2>Major changes to included software</h2>

<ul>
<li>
Replace <em>KeePassX</em> with <a href="https://keepassxc.org/" rel="nofollow"><em>KeePassXC</em></a>, which is more actively developed.
</li>
<li>
Update <em>OnionShare</em> from 0.9.2 to 1.3.2, which includes a lot of usability improvements.
</li>
</ul>

<p><a rel="nofollow"></a></p>

<ul>
<li>
Update <em>Tor Browser</em> to 9.0:
<ul>
<li>
A gray border, called <em>letter boxing</em>, is now displayed around the content of web pages when you resize the window of Tor Browser.<br />
Letter boxing prevents websites from identifying your browser based on the size of its window. Letter boxing replaces the yellow warning that was displayed until now when maximizing Tor Browser.
</li>
<li>
The onion icon has been removed from the top bar.<br />
To switch to a new identity, choose <img src="../lib/open-menu.png" width="16" alt="" /> ▸ New Identity.
</li>
</ul>
</li>
</ul>

<p><a rel="nofollow"></a></p>

<ul>
<li>
Update <em>MAT</em> from 0.6.1 to <a href="https://0xacab.org/jvoisin/mat2/blob/master/CHANGELOG.md" rel="nofollow">0.8.0</a><br />
<em>MAT</em> has no graphical interface of its own anymore.<br />
To clean the metadata of a file:
<ol>
<li>
Open the <em>Files</em> browser and navigate to the file that you want to clean.
</li>
<li>
Right-click (on Mac, click with two fingers) on the file.
</li>
<li>
Choose <strong>Remove metadata</strong>.
</li>
</ol>
</li>
<li>
Update <em>Linux</em> to 5.3.2. This should also improve the support for newer hardware (graphics, Wi-Fi, etc.).
</li>
</ul>

<p><a rel="nofollow"></a></p>

<ul>
<li>
Update <em>Electrum</em> from 3.2.3 to <a href="https://github.com/spesmilo/electrum/blob/master/RELEASE-NOTES" rel="nofollow">3.3.8</a>. <em>Electrum</em> works again in Tails.
</li>
<li>
Update <em>Enigmail</em> to 2.0.12 and <em>gnupg</em> to 2.2.12, which mitigate <a href="https://dkg.fifthhorseman.net/blog/openpgp-certificate-flooding/" rel="nofollow">OpenPGP certificate flooding</a>.
</li>
<li>
Upgrade most other software, for example:
<ul>
<li>
<em>Audacity</em> from 2.1.2 to <a href="https://wiki.audacityteam.org/wiki/Audacity_Versions#whatsnew" rel="nofollow">2.2.2</a>
</li>
<li>
<em>GIMP</em> from 2.8.18 to <a href="https://docs.gimp.org/2.10/en/gimp-introduction-whats-new/" rel="nofollow">2.10.8</a>
</li>
<li>
<em>Inkscape</em> from 0.92.1 to <a href="https://inkscape.org/release/inkscape-0.92.4/" rel="nofollow">0.92.4</a>
</li>
<li>
<em>LibreOffice</em> from 5.2.7 to <a href="https://wiki.documentfoundation.org/ReleaseNotes/6.0" rel="nofollow">6.1.5</a>
</li>
<li>
<em>git</em> from 2.11.0 to 2.20.1
</li>
<li>
<em>Tor</em> to 0.4.1.6
</li>
</ul>
</li>
<li>
Remove <em>Scribus</em>.<br />
You can install <em>Scribus</em> again using the <em><a href="https://tails.boum.org/doc/first_steps/additional_software/" rel="nofollow">Additional Software</a></em> feature.
</li>
</ul>

<h2>Usability improvements to <em>Tails Greeter</em></h2>

<p>We improved various aspects of the usability of <em>Tails Greeter</em>, especially for non-English users:</p>

<ul>
<li>
To make it easier to select a language, we curated the list of proposed languages by removing the ones that had too little translations to be useful. We also clarified how Chinese is listed by having different entries for simplified and traditional Chinese.
</li>
<li>
We simplified the list of keyboard layouts.
</li>
<li>
We fixed the <strong>Formats</strong> setting, which was not being applied.
</li>
<li>
We prevented additional settings to be applied when clicking on <strong>Cancel</strong> or <strong>Back</strong>.
</li>
<li>
We fixed the opening of help pages in other languages than English, when available.
</li>
</ul>

<h2>Performance and usability improvements</h2>

<ul>
<li>
Tails 4.0 starts 20% faster.
</li>
<li>
Tails 4.0 requires about 250 MB less of RAM.
</li>
<li>
Tails 4.0 is 47 MB smaller to download than Tails 3.16, despite all these changes.
</li>
<li>
Add support for <em>Thunderbolt</em> devices.
</li>
<li>
The screen keyboard is easier to use.
</li>
<li>
Make it possible to show the password of the persistent storage when creating one.
</li>
<li>
Add support for USB tethering from iPhone.
</li>
</ul>

<h2>New documentation pages</h2>

<ul>
<li>
<a href="https://tails.boum.org/doc/encryption_and_privacy/secure_deletion/#erase-device" rel="nofollow">How to securely erasing an entire device</a>, including USB sticks and SSDs.
</li>
<li>
<a href="https://tails.boum.org/doc/first_steps/persistence/copy/" rel="nofollow">How to backup your persistent volume</a>.
</li>
</ul>

<h2>Other changes</h2>

<ul>
<li>
Use the default bookmarks from <em>Tor Browser</em> instead of our own default bookmarks. (<a href="https://redmine.tails.boum.org/code/issues/15895" rel="nofollow">#15895</a>)
</li>
<li>
Remove the <em>Home</em> launcher from the desktop. (<a href="https://redmine.tails.boum.org/code/issues/16799" rel="nofollow">#16799</a>)
</li>
<li>
Remove the default accounts in <em>Pidgin</em>. (<a href="https://redmine.tails.boum.org/code/issues/16744" rel="nofollow">#16744</a>)
</li>
</ul>

<h1>Fixed problems</h1>

<p>For more details, read our <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">changelog</a>.</p>

<ul>
<li>
Allow opening persistent volumes from other Tails USB sticks. (<a href="https://redmine.tails.boum.org/code/issues/16789" rel="nofollow">#16789</a>)
</li>
<li>
Fix the delivery of WhisperBack reports. (<a href="https://redmine.tails.boum.org/code/issues/17110" rel="nofollow">#17110</a>)
</li>
</ul>

<p><a rel="nofollow"></a></p>

<h1>Known issues</h1>

<p>None specific to this release.<br />
See the list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">long-standing issues</a>.</p>

<h1>Get Tails 4.0</h1>

<h2>To upgrade your Tails USB stick and keep your persistent storage</h2>

<p>Automatic upgrades are not available to 4.0.<br />
All users must do a <a href="https://tails.boum.org/upgrade/" rel="nofollow">manual upgrade</a>.</p>

<h2>To install Tails on a new USB stick</h2>

<p>Follow our installation instructions:</p>

<ul>
<li><a href="https://tails.boum.org/install/win/" rel="nofollow">Install from Windows</a></li>
<li><a href="https://tails.boum.org/install/mac/" rel="nofollow">Install from macOS</a></li>
<li><a href="https://tails.boum.org/install/linux/" rel="nofollow">Install from Linux</a></li>
</ul>

<p>All the data on this USB stick will be lost.</p>

<h2>To download only</h2>

<p>If you don't need installation or upgrade instructions, you can directly download Tails 4.0:</p>

<ul>
<li><a href="https://tails.boum.org/install/download/" rel="nofollow">For USB sticks (USB image)</a></li>
<li><a href="https://tails.boum.org/install/download-iso/" rel="nofollow">For DVDs and virtual machines (ISO image)</a></li>
</ul>

<h1>What's coming up?</h1>

<p>Tails 4.1 is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for December 3.<br />
Have a look at our <a href="https://tails.boum.org/contribute/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.<br />
We need your help and there are many ways to <a href="https://tails.boum.org/contribute/" rel="nofollow">contribute to Tails</a> (<a href="https://tails.boum.org/donate/?r=4.0" rel="nofollow">donating</a> is only one of them). Come <a href="https://tails.boum.org/about/contact/#tails-dev" rel="nofollow">talk to us</a>!</p>

<h1>Support and feedback</h1>

<p>      For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

